import React, { useState} from 'react'
import { Drawer, List, ListItem, ListItemText } from '@material-ui/core'

export const DrawerComponent = ({ openDrawer, setOpenDrawer}) => {

    
  return (
    <Drawer onClose={() => setOpenDrawer(prev => !prev)} open={openDrawer}>
        <List>
            <ListItem>
                <ListItemText>Home</ListItemText>
                

                </ListItem>
            <ListItem>
                <ListItemText>About</ListItemText>
                

                </ListItem>
            <ListItem>
                <ListItemText>Project</ListItemText>
                

                </ListItem>

        </List>

    </Drawer>
  )
}
