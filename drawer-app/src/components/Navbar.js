import React, { useState} from 'react'
import { DrawerComponent} from './DrawerComponent'

export const Navbar = () => {

    const [openDrawer, setOpenDrawer] = useState(true)
  return (
    <div>
        { !openDrawer && <button onClick={() => setOpenDrawer(prev => !prev)}>Open Drawer</button> }
        <DrawerComponent setOpenDrawer={setOpenDrawer} openDrawer={openDrawer}/>

    </div>
  )
}
